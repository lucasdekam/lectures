---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.0'
      jupytext_version: 0.8.6
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python tags=["initialize"]
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import quad

from common import draw_classic_axes, configure_plotting

configure_plotting()
```

# Solutions for lecture 2 exercises

### warm-up exercises

1. For low T, $1/T \rightarrow \infty$. The heat capacity is then given as:
$$
C \overset{\mathrm{low \: T}}{\approx} 9Nk_{\mathrm{B}}\left(\frac{T}{T_{D}}\right)^3\int_0^{\infty}\frac{x^4{\mathrm{e}}^x}{({\mathrm{e}}^x-1)^2}{\mathrm{d}}x.
$$
2. See plot below (shown for $T_{D,1} < T_{D,2}$)
3. The polarization is related to the direction of the amplitudes of the waves with respect to the direction of the wave.
In 3D, there are only 3 different amplitude directions possible.
4. 
$$
\int k_x k_y \rightarrow \int_{0}^{2\pi} \mathrm{d} \theta \int_{0}^{\infty} k \mathrm{d} k = 2\pi \int_{0}^{\infty} k \mathrm{d} k 
$$
5. The Debye frequency $\omega_D$.
6. The wavelength is of the order of the interatomic spacing:
$$
\lambda = (\frac{4}{3}\pi)^{1/3} a.
$$

```python
fig, ax = plt.subplots()
T = np.linspace(0.1, 3)
T_D = [1,2]
ax.plot(T, (T/T_D[0])**3, 'b-', label = r'$T_{D,1}$')
ax.plot(T, (T/T_D[1])**3, 'r-', label = r'$T_{D,2}$')
ax.set_ylim([0,3])
ax.set_xlim([0,3])
ax.set_xlabel('$T$')
ax.set_xticks([0])
ax.set_xticklabels(['$0$'])
ax.set_ylabel('$C$')
ax.set_yticks([0])
ax.set_yticklabels(['$0$'])
ax.legend();
```

### Exercise 1: Debye model: concepts.

1. $k = \frac{4\pi}{L}$ and $k = -\frac{4\pi}{L}$.  
2. 
3. The number of states per $k$ or per frequency.
4. 
$$
g(\omega) = \frac{dN}{d\omega} = \frac{dN}{dk}\frac{dk}{d\omega} = \frac{1}{v}\frac{dN}{dk}.
$$
We assume that in $d$ dimensions there are $d$ polarizations.

For 1D we have that $N = \frac{L}{2\pi}\int_{-k}^{k} dk$, hence $g(\omega) = \frac{L}{\pi v}$.

For 2D we have that $N = 2\left(\frac{L}{2\pi}\right)^2\int d^2k = 2\left(\frac{L}{2\pi}\right)^2\int 2\pi kdk$, hence $g(\omega) = \frac{L^2\omega}{\pi v^2}$.

For 3D we have that $N = 3\left(\frac{L}{2\pi}\right)^3\int d^3k = 3\left(\frac{L}{2\pi}\right)^3\int 4\pi k^2dk$, hence $g(\omega) = \frac{3L^3\omega^2}{2\pi^2v^3}$.

###  Exercise 2: Debye model in 2D.

1. See lecture notes.
2. $$
\begin{align}
E &= \int_{0}^{\omega_D}g(\omega)\hbar\omega\left(\frac{1}{e^{\beta\hbar\omega} - 1} + \frac{1}{2}\right)d\omega \\
&= \frac{L^2}{\pi v^2\hbar^2\beta^3}\int_{0}^{\beta\hbar\omega_D}\frac{x^2}{e^{x} - 1}dx + C.
\end{align}
$$
3. High temperature implies $\beta \rightarrow 0$, hence $E = \frac{L^2}{\pi v^2\hbar^2\beta^3}\frac{(\beta\hbar\omega_D)^2}{2} + C$, and then $C = \frac{k_BL^2\omega^2_D}{2\pi v^2} = 2Nk_B$. We've used the value for $\omega_D$ calculated from $2N = \int_{0}^{\omega_D}g(\omega)d\omega$.
4. In the low temperature limit we have that $\beta \rightarrow \infty$, hence $E \approx \frac{L^2}{\pi v^2\hbar^2\beta^3}\int_{0}^{\infty}\frac{x^2}{e^{x} - 1}dx + C = \frac{2\zeta(3)L^2}{\pi v^2\hbar^2\beta^3} + C$. Finally $C = \frac{6\zeta(3)k^3_BL^2}{\pi v^2\hbar^2}T^2$. We used the fact that $\int_{0}^{\infty}\frac{x^2}{e^{x} - 1}dx = 2\zeta(3)$ where $\zeta$ is the Riemann zeta function.

###  Exercise 3: Different phonon modes.
1. $$
g(\omega) = \sum_{\text{polarizations}}\frac{dN}{dk}\frac{dk}{d\omega} = \left(\frac{L}{2\pi}\right)^3\sum_{\text{polarizations}}4\pi k^2\frac{dk}{d\omega} = \frac{L^3}{2\pi^2}\left(\frac{2}{v_\perp^3} + \frac{1}{v_\parallel^3}\right)\omega^2
$$
$$
E = \int_{0}^{\omega_D}g(\omega)\hbar\omega\left(\frac{1}{e^{\beta\hbar\omega} - 1} + \frac{1}{2}\right)d\omega = \frac{L^3}{2\pi^2\hbar^3\beta^4}\left(\frac{2}{v_\perp^3} + \frac{1}{v_\parallel^3}\right)\int_{0}^{\beta\hbar\omega_D}\frac{x^3}{e^{x} - 1}dx + C.
$$

2. Note that we can get $\omega_D$ from $3N = \int_{0}^{\omega_D}g(\omega)$ so everything cancels as usual and we are left with the Dulong-Petit law $C = 3Nk_B$.
3. In the low temperature limit we have that $C \sim \frac{2\pi^2k_B^4L^3}{15\hbar^3}\left(\frac{2}{v_\perp^3} + \frac{1}{v_\parallel^3}\right)T^3$. We used that $\int_{0}^{\infty}\frac{x^3}{e^{x} - 1}dx = \frac{\pi^4}{15}$.

### Exercise 4: Anisotropic sound velocities.

$$
E = 3\left(\frac{L}{2\pi}\right)^3\int d^3k\hbar\omega(\mathbf{k})\left(n_B(\beta\hbar\omega(\mathbf{k})) + \frac{1}{2}\right) = 3\left(\frac{L}{2\pi}\right)^3\frac{1}{v_xv_yv_z}\int d^3\kappa\frac{\hbar\kappa}{e^{\beta\hbar\kappa} - 1} + C,
$$

where we used the substitutions $\kappa_x = k_xv_x,\kappa_y = k_yv_y, \kappa_z = k_zv_z$. Finally

$$
E = \frac{3\hbar L^3}{2\pi^2}\frac{1}{v_xv_yv_z}\int_0^{\kappa_D} d\kappa\frac{\kappa^3}{e^{\beta\hbar\kappa} - 1} + C = \frac{3L^3}{2\pi^2\hbar^3\beta^4}\frac{1}{v_xv_yv_z}\int_0^{\beta\hbar\kappa_D} dx\frac{x^3}{e^{x} - 1} + C,
$$

hence $C = \frac{\partial E}{\partial T} = \frac{6k_B^4L^3T^3}{\pi^2\hbar^3}\frac{1}{v_xv_yv_z}\int_0^{\beta\hbar\kappa_D} dx\frac{x^3}{e^{x} - 1}$. We see that the result is similar to the one with the linear dispersion, the only difference is the factor $1/v_xv_yv_z$ instead of $1/v^3$.
