---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.0'
      jupytext_version: 1.0.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python tags=["initialize"]
import matplotlib.pyplot as plt

import numpy as np
from math import sqrt,pi

from common import draw_classic_axes, configure_plotting

import plotly.offline as py
from plotly.subplots import make_subplots
import plotly.graph_objs as go

configure_plotting()

py.init_notebook_mode(connected=True)
```

# Lecture 9 – Crystal structure

_based on chapter 12 of the book_  


!!! success "Expected prior knowledge"

    Before the start of this lecture, you should be able to:

    - use elementary vector calculus

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Describe any crystal using crystallographic terminology, and interpret this terminology
    - Compute the volume filling fraction given a crystal structure
    - Determine the primitive, conventional, and Wigner-Seitz unit cells of a given lattice
    - Determine the Miller planes of a given lattice

??? info "Lecture video"

    <iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/zl8htOplU1s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Crystal classification

```python
# Define the lattice vectors
a1 = np.array([1,0])
a2 = np.array([0.5,sqrt(3)/2])
a1_alt = -a1-a2
a2_alt = a1_alt + a1
a2_c = np.array([0,sqrt(3)])

# Produces the lattice to be fed into plotly
def lattice_generation(a1,a2,N=6):
    grid = np.arange(-N//2,N//2,1)
    xGrid, yGrid = np.meshgrid(grid,grid)
    return np.reshape(np.kron(xGrid.flatten(),a1),(-1,2))+np.reshape(np.kron(yGrid.flatten(),a2),(-1,2))


# Produces the dotted lines of the unit cell
def dash_contour(a1,a2, vec_zero = np.array([0,0]), color='Red'):
    dotLine_a1 = np.transpose(np.array([a1,a1+a2])+vec_zero)
    dotLine_a2 = np.transpose(np.array([a2,a1+a2])+vec_zero)
    
    def dash_trace(vec,color): 
        trace = go.Scatter(
            x=vec[0], 
            y=vec[1], 
            mode = 'lines', 
            line_width = 2, 
            line_color = color, 
            line_dash='dot',
            visible=False
        )
        return trace
    
    return dash_trace(dotLine_a1,color),dash_trace(dotLine_a2,color)

# Makes the lattice vector arrow
def make_arrow(vec,text,color = 'Red',vec_zero = [0,0],text_shift = [-0.2,-0.1]):
    annot = [dict(
            x=vec[0]+vec_zero[0],
            y=vec[1]+vec_zero[1],
            ax=vec_zero[0],
            ay=vec_zero[1],
            xref='x',
            yref='y',
            axref='x',
            ayref = 'y',
            showarrow=True,
            arrowhead=2,
            arrowsize=1,
            arrowwidth=3, 
            arrowcolor = color
                 ),
         dict(
            x=(vec[0]+vec_zero[0])/2+text_shift[0],
            y=(vec[1]+vec_zero[1])/2+text_shift[1],
            xref='x',
            ayref = 'y',
            text = text,
            font = dict(
                color = color,
                size = 20
            ),
            showarrow=False,
             )
        ]
    return annot

# Create the pattern
pattern_points = lattice_generation(a1,a2)
pattern = go.Scatter(x=pattern_points.T[0],y=pattern_points.T[1],mode='markers',marker=dict( 
    color='Black', 
    size = 20,
    symbol = 'star-open-dot')
    )

# Lattice Choice A
latticeA = go.Scatter(visible = False,x=pattern_points.T[0],y=pattern_points.T[1],mode='markers',marker=dict(
        color='Red',
        size = 10,
        )
    )

# Lattice Choice B
latB_points = lattice_generation(a1,a2)
latticeB = go.Scatter(visible = False, x=latB_points.T[0]+0.5,y=latB_points.T[1],mode='markers',marker=dict(
        color='Blue',
        size = 10,
        )
    )

# Annotate the lattice vectors
# Button 2 
bt2_annot = make_arrow(a1,'$a_1$') + make_arrow(a2,'$a_2$',text_shift=[-0.3,-0.1]) + make_arrow(a1_alt,'$a`_1$',color='Black',text_shift=[-0.6,-0.1]) + make_arrow(a2_alt,'$a`_2$',color='Black',text_shift=[-0.35,-0.1])
#Button 3
bt3_annot =  make_arrow(a1,'$a_1$', vec_zero = [-0.5,0], color = 'Blue') + make_arrow(a2,'$a_2$',vec_zero = [-0.5,0], color = 'Blue')
#Button 4
bt4_annot =  make_arrow(a2_c,'$a_2$') + make_arrow(a1,'$a_1$')

# (0)  Button 1, (0, 1-5) Button 2, (0, 6-8) Button 3, (0,1,9,10)
data = [pattern,latticeA,*dash_contour(a1,a2),*dash_contour(a1_alt,a2_alt,color='Black'),
        latticeB, *dash_contour(a1,a2,vec_zero=[-0.5,0],color='Blue'), 
        *dash_contour(a1, a2_c)]


updatemenus=list([ 
    dict(
        type="buttons",
        direction="down",
        active=0,
        buttons=list([
            dict(label="Pattern",
                 method="update",
                 args=[{"visible": [True, False, False, False, False, False, False, False, False, False, False]},
                       {"title": "Pattern",
                        "annotations": []}]),
            dict(label="Lattice A",
                 method="update",
                 args=[{"visible": [True, True, True, True, True, True, False, False, False, False, False]},
                       {"title": "Lattice A with a Primitive Unit Cell",
                        "annotations": bt2_annot}]),
            dict(label="Lattice B",
                 method="update",
                 args=[{"visible": [True, False, False, False, False, False, True, True, True, False, False]},
                       {"title": "Lattice B with a Primitive Unit Cell",
                        "annotations": bt3_annot}]),
            dict(label="C.U.Cell",
                 method="update",
                 args=[{"visible": [True, True, False, False, False, False, False, False, False, True, True]},
                       {"title": "Conventional Unit Cell",
                        "annotations": bt4_annot}])
        ]),
    )
]
)


plot_range = 2
axis = dict(
        range=[-plot_range,plot_range],
        visible = False,
        showgrid = False,
        fixedrange = True
    )

layout = dict(
    showlegend = False,
    updatemenus=updatemenus,
    plot_bgcolor = 'rgb(254, 254, 254)',
    width = 600,
    height = 600,
    xaxis = axis,
    yaxis = axis
)


fig=dict(data=data, layout=layout)
py.iplot(fig, filename='lattice_basics')

```

Most of solid state physics deals with crystals, which are periodic structures. To describe a periodic structure, we need a simple framework. Such a framework is given by the concept of a **lattice**:

> A **lattice** is an infinite set of points defined by integer sums of a set of linearly independent **primitive lattice vectors**.
$$
\begin{equation}
\mathbf{R}_{\left[n_{1} n_{2} n_{3}\right]}=n_{1} \mathbf{a}_{1}+n_{2} \mathbf{a}_{2}+n_{3} \mathbf{a}_{3} \quad n_{1}, n_{2}, n_{3} \in \mathbb{Z}
\end{equation}
$$

That is pretty rigorous. A more practical, equivalent definition is: 

> A **lattice** is a set of points where the environment of any given point is equivalent to the environment of any other given point.

Lets put these into practice. In the 'Pattern' tab of the plot above, a simple $\star$ pattern is presented. 

There are several ways to assign lattice points to the plot. One such option is given in the plot's 'Lattice A' tab. As the first definition suggests, we can define **primitive lattice vectors** connecting these lattice points. Again, the choice of primitive lattice vectors is not unique given a lattice. Both the black and red vectors are equally good definitions since their integer linear combinations are able to map out all of the lattice points (try it out!)

Does the choice of a lattice need to coincide with the centre of our arbitrary $\star$ pattern? Nope! In the 'Lattice B' tab, we can see another choice of the lattice. One can confirm that around each lattice point the surroundings are the same, fulfilling our second definition of a lattice. Since the lattice points are shifted from the $\star$, we need to include that information in order to reconstruct the pattern from the lattice alone. This can be done using the concept of a **basis**

> The description of objects with respect to the reference lattice point is known as a **basis**.

The reference lattice point is the chosen lattice point to which we apply the lattice vectors in order to reconstruct the lattice.
In the 'Lattice A' case, the basis is trivial $\star = (0,0)$ since each lattice point corresponds with the $\star$ object. In 'Lattice B', each object is shifted by $0.5a_1$ away from the lattice point. Therefore, its basis is $\star = (1/2,0)$. It is expressed in terms of **fractional coordinates** which depends on your definition of lattice vectors. For example, $\star = (1/2,0)$ implies that the position of $\star$ is at $(1/2)\mathbf{a}_{1}+0\mathbf{a}_{2}$. The basis is especially important when a crystal has many different types of atoms (for example many salts like NaCl)

Rather than work with the whole space of the crystal, it is practical to use the smallest possible 'building block' of the crystal - a **unit cell**: 

> A **unit cell** is a region of space such that when many unit cells are stacked together it tiles (completely fills) all of space and reconstructs the full structure.

The most straightforward way to construct a unit cell is through the **primitive lattice vectors** as shown in the 'Lattice A' and 'Lattice B' tabs via the extended dotted lines. Such a construction is known as a **primitive unit cell** since it contains only 1 lattice point. One can confirm this by noting that each primitive unit cell shares its corner lattice points with 4 neighbouring cells. Therefore, $\frac{1}{4} \times 4 = 1$, confirming our definition.

A primitive unit cell might not be the most practical choice due to non-orthogonal axes. However, we can always define a **conventional unit cell** with an orthogonal set of lattice vectors as shown in 'C.U.Cell' tab. Conventional unit cells can contain multiple lattice points, as can be seen in the plot: since there is an additional lattice point at the centre, there are $1 + \frac{1}{4} \times 4 = 2 $ lattice points. There is a slight problem with this definition of lattice vectors - no integer linear combination of lattice vectors is able to produce the centre lattice points in the unit cell. In order to be able to reproduce the pattern, we need to include that information and the easiest way to do so is through our definition of a basis. Since there is one $\star$ at the corner of the unit cell, and one at the centre, the basis is $\star = (0,0),(1/2,1/2)$. Alternatively, one can say that the complete crystal structure is made up from two interpenetrating orthogonal lattices - one centred at $(0,0)$ and another at $(1/2,1/2)$, each with basis $\star = (0,0)$ 

### Example: analyzing the graphene crystal structure

![](figures/graphite_mod.svg)

1. Choose origin (can be atom, not necessary)
2. Find other lattice points that are identical
3. Choose lattice vectors, either primitive (red) or not primitive (blue)
    - lengths of lattice vectors and angle(s) between them fully define the crystal lattice
    - for graphite: $|{\bf a}_1|=|{\bf a}_2|$ = 0.246 nm = 2.46 Å, $\gamma$ = 60$^{\circ}$
4. Specify basis (we use the lattice coordinates)
    - using ${\bf a}_1$ and ${\bf a}_2$: C$(0,0)$, C$\left(\frac{2}{3},\frac{2}{3}\right)$
    - using ${\bf a}_1$ and ${\bf a}_{2}'$: C$(0,0)$, C$\left(0,\frac{1}{3}\right)$, C$\left(\frac{1}{2},\frac{1}{2}\right)$, C$\left(\frac{1}{2},\frac{5}{6}\right)$

An alternative type of unit cell is the _Wigner-Seitz cell_: the collection of all points that are closer to one specific lattice point than to any other lattice point. You form this cell by taking all the perpendicular bisectrices or lines connecting a lattice point to its neighboring lattice points.

### Stacking of atoms
What determines what crystal structure a material adopts? $\rightarrow$ To good approximation, atoms are solid incrompressible spheres that attract each other. How will these organize?

We start with the densest possible packing in 2D:
![](figures/packing.svg)
Will the second layer go on sites A, B or C?

ABCABC stacking $\rightarrow$ _cubic close packed_, also known as _face centered cubic_ (fcc):

![](figures/stacking.svg)




```python
X = np.linspace(0, 1, 100)
Y = np.linspace(0, 1, 100)
X, Y = np.meshgrid(X, Y)

Z1 = 1 - X - Y
surface1 = go.Surface(x=X, y=Y, z=Z1, opacity=0.7, showscale=False,cmax=1,cmin=0)

Z2 = 2 - X - Y        
surface2 = go.Surface(x=X, y=Y, z=Z2, opacity=0.7, showscale=False,cmax=1,cmin=0)


VEC_BLACK = [
    [0,0,0],[1,1,1]
]
VEC_RED = [
    [1,0,0],[0,1,0],[0,0,1],[0.5,0.5,0],[0.5,0,0.5],[0,0.5,0.5]
]
VEC_BLUE = [
    [1,1,0],[0,1,1],[1,0,1],[0.5,0.5,1],[0.5,1,0.5],[1,0.5,0.5]
]

COLORS = ['rgb(0,0,0)','rgb(33,64,154)','rgb(255,0,0)']

def scatter(vec_list,color_code):
    vec_list = np.transpose(vec_list)
    trace=go.Scatter3d(x=vec_list[0],
                   y=vec_list[1],
                   z=vec_list[2],
                   mode = 'markers',
                   marker = dict(
                            sizemode = 'diameter',
                            sizeref = 20,
                            size = 30,
                            color = color_code
                           )
                         )
    return trace

x_axis = go.Scatter3d(x=[0,1],
                   y=[0,0],
                   z=[0,0],
                   mode = 'lines',
                   line_width = 10,
                   line_color = 'Black'
                     )

y_axis = go.Scatter3d(x=[0,0],
                   y=[0,1],
                   z=[0,0],
                   mode = 'lines',
                   line_width = 10,
                   line_color = 'Black'
                     )


z_axis = go.Scatter3d(x=[0,0],
                   y=[0,0],
                   z=[0,1],
                   mode = 'lines',
                   line_width = 10,
                   line_color = 'Black'
                     )

data = [scatter(i,COLORS[index]) for index,i in enumerate([VEC_BLACK,VEC_RED,VEC_BLUE])]
data.append(surface1)
data.append(surface2)
data.append(x_axis)
data.append(y_axis)
data.append(z_axis)


scene_axis_settings = lambda axis_name : dict(
                        title = axis_name,
                        range = [0,1],
                        ticks ='',
                        showbackground = False,
                        showgrid = False,
                        showline = False,
                        showspikes = False,
                        showticklabels = False
                        )

layout=go.Layout(showlegend = False,
                 scene = dict(
                        aspectmode= 'cube',
                        xaxis=scene_axis_settings('X'),
                        yaxis=scene_axis_settings('Y'),
                        zaxis=scene_axis_settings('Z')
                    )
                )
fig=go.Figure(data=data,layout=layout)
py.iplot(fig, filename='FCC')
```

- One atom on the center of each side-plane: 'a die that always throws 1'
- Conventional unit cell $\neq$ primitive unit cell
- Cyclic ABC $\rightarrow$ all atoms identical $\rightarrow$ 1 atom per primitive unit cell
- Conventional cell: $8\times\frac{1}{8}+6\times\frac{1}{2}=1+3=4$ atoms

Examples of fcc crystals: Al, Cu, Ni, Pb, Au and Ag.

### Filling factor
Filling factor = # of atoms per cell $\times$ volume of 1 atom / volume of cell

$=\frac{4\times\frac{4}{3}\pi R^3}{a^3}=\frac{1}{6}\sqrt{2}\pi\approx 0.74$, where we used $\sqrt{2}a=4R$.

Compare this to _body centered cubic_ (bcc), which consists of a cube with atoms on the corners and one atom in the center (Fig. 12.20 of the book): filling factor = 0.68. Examples of bcc crystals: Fe, Na, W, Nb.

Question: is 74% the largest possible filling factor? $\rightarrow$ Kepler conjecture (1571 – 1630). Positive proof by Hales _et al._ in 1998!

Crystal structures with an fcc lattice:

1. ionic crystals are usually fcc. E.g. NaCl (Fig. 12.20 )
2. zincblende & diamond crystals (Fig. 12.20)

ABABAB stacking $\rightarrow$ _hexagonally close-packed_ (hcp), e.g. Co, Zn. In this case there is no cubic symmetry (Fig. 1.11).

### Miller planes
We start with a simple cubic lattice:

![](figures/cubic_mod.svg)

$|{\bf a}_1|=|{\bf a}_2|=|{\bf a}_3|\equiv a$ (_lattice constant_)

The plane designated by Miller indices $(u,v,w)$ intersects lattice vector ${\bf a}_1$ at $\frac{|{\bf a}_1|}{u}$, ${\bf a}_2$ at $\frac{|{\bf a}_2|}{v}$ and ${\bf a}_3$ at $\frac{|{\bf a}_3|}{w}$.

![](figures/miller.svg)

Miller index 0 means that the plane is parallel to that axis (intersection at "$\frac{|{\bf a}_3|}{0}=\infty$"). A bar above a Miller index means intersection at a negative coordinate.

If a crystal is symmetric under $90^\circ$ rotations, then $(100)$, $(010)$ and $(001)$ are physically indistinguishable. This is indicated with $\{100\}$. $[100]$ is a vector. In a cubic crystal, $[100]$ is perpendicular to $(100)$ $\rightarrow$ proof in problem set.

## Summary
* We introduced several important concepts that allow us to describe crystal structure: lattice, lattice vectors, basis, primitive & conventional unit cells, and Miller planes.
* We introduced several common lattices: simple-cubic, FCC, and BCC (3D), triangular (2D).
* We discussed how to compute a filling factor.
* A crystal is constructed by by placing the basis at each point in a lattice.

## Exercises

### Quick warm-up exercises

1. Draw a unit cell of an FCC, a BCC, and a triangular lattice. Are the unit cells you drew primitive? 
2. How many lattice points are there in a primitive unit cell?
3. Suppose you found the primitive unit cell of a diatomic crystal. How many basis vectors do you minimally need to describe the crystal? Can some diatomic crystals require more basis vectors?
4. Calculate the filling factor of a simple cubic lattice.
5. Sketch the $(110),(1\bar{1}0),(111)$ miller planes of a simple cubic lattice.


### Exercise 1: Diatomic crystal

Consider the following two-dimensional diatomic crystal:

```python
y = np.repeat(np.arange(0,8,2),4)
x = np.tile(np.arange(0,8,2),4)
plt.figure(figsize=(5,5))
plt.axis('off')

plt.plot(x,y,'ko', markersize=15)
plt.plot(x+1,y+1, 'o', markerfacecolor='none', markeredgecolor='k', markersize=15);
```

1. What is the definition of a primitive unit cell? Sketch a Wigner-Seitz unit cell and two other possible primitive unit cells of the crystal. 
2. If the distance between the filled cirles is $a=0.28$ nm, what is the volume of the primitive unit cell? How would this volume change if all the empty circles and the filled circles were identical?
3. Write down one set of primitive lattice vectors and the basis for this crystal.
4. Imagine expanding the lattice into the perpendicular direction $z$. We can define a new three-dimensional crystal by considering a periodic structure in the $z$ direction, where the filled circles have been displaced by $\frac{a}{2}$ from the empty circles. The following figure shows the new arrangement of the atoms. What lattice do we obtain? Write down the basis of the three-dimensional crystal and give an example of this type of crystal. 

```python
x = np.tile(np.arange(0,2,1),4)
y = np.repeat(np.arange(0,2,1),4)
z = np.tile(np.repeat(np.arange(0,2,1),2),2)

trace1 = go.Scatter3d(
    x = x,
    y = y,
    z = z,
    mode = 'markers',
    marker = dict(
        sizemode = 'diameter',
        sizeref = 20,
        size = 20,
        color = 'rgb(255, 255, 255)',
        line = dict(
          color = 'rgb(0,0,0)',
          width = 5
        )
        )
)

trace2 = go.Scatter3d(
    x = [0.5],
    y = [0.5],
    z = [0.5],
    mode = 'markers',
    marker = dict(
        sizemode = 'diameter',
        sizeref = 20,
        size = 20,
        color = 'rgb(0,0,0)',
        line = dict(
          color = 'rgb(0,0,0)',
          width = 5
        )
        )
)

data=[trace1, trace2]

layout=go.Layout(showlegend = False,
                scene = dict(
                        xaxis=dict(
                        title= 'x[a]',
                        ticks='',
                        showticklabels=False
                        ),
                        yaxis=dict(
                        title= 'y[a]',
                        ticks='',
                        showticklabels=False
                        ),
                        zaxis=dict(
                        title= 'z[a]',
                        ticks='',
                        showticklabels=False
                        )
                    ))


fig=go.Figure(data=data, layout=layout)
py.iplot(fig, show_link=False)
```

5. If we consider all atoms to be the same, what lattice do we obtain? Give an example of this type of crystal.
6. Compute the filling factor of the three-dimensional crystal. Choose the radius of the atoms to be such that the nearest neighbouring atoms just touch.

### Exercise 2: Diamond lattice

Consider a the [diamond crystal structure](https://en.wikipedia.org/wiki/Diamond_cubic) structure. The following illustration shows the arrangement of the carbon atoms in a conventional unit cell.

```python
Xn = np.tile(np.arange(0,2,1),4)
Yn = np.repeat(np.arange(0,2,1),4)
Zn = np.tile(np.repeat(np.arange(0,2,1),2),2)

Xn = np.hstack((Xn, Xn, Xn+0.5, Xn+0.5))
Yn = np.hstack((Yn, Yn+0.5, Yn+0.5, Yn))
Zn = np.hstack((Zn, Zn+0.5, Zn, Zn+0.5))

Xn = np.hstack((Xn, Xn+1/4))
Yn = np.hstack((Yn, Yn+1/4))
Zn = np.hstack((Zn, Zn+1/4))

Xe=[]
Ye=[]
Ze=[]

num_atoms = len(Xn)
for i in range(num_atoms):
    for j in np.arange(i+1, num_atoms, 1):
        pos1 = np.array([Xn[i], Yn[i], Zn[i]])
        pos2 = np.array([Xn[j], Yn[j], Zn[j]])
        if np.linalg.norm(pos1-pos2) == sqrt(3)/4:
            Xe+=[Xn[i], Xn[j], None]
            Ye+=[Yn[i], Yn[j], None]  
            Ze+=[Zn[i], Zn[j], None]  
    
trace1=go.Scatter3d(x=Xe,
               y=Ye,
               z=Ze,
               mode='lines',
               line=dict(color='rgb(0,0,0)', width=3),
               hoverinfo='none'
                )

trace2=go.Scatter3d(x=Xn,
               y=Yn,
               z=Zn,
               mode = 'markers',
               marker = dict(
                        sizemode = 'diameter',
                        sizeref = 20,
                        size = 20,
                        color = 'rgb(255,255,255)',
                        line = dict(
                               color = 'rgb(0,0,0)',
                               width = 5
                               )
                       )
                     )
 
layout=go.Layout(showlegend = False,
                scene = dict(
                        xaxis=dict(
                        title= 'x[a]',
                        range= [0,1],
                        ticks='',
                        showticklabels=False
                        ),
                        yaxis=dict(
                        title= 'y[a]',
                        range= [0,1],
                        ticks='',
                        showticklabels=False
                        ),
                        zaxis=dict(
                        title= 'z[a]',
                        range= [0,1],
                        ticks='',
                        showticklabels=False
                        )
                    ))

data=[trace1, trace2]

fig=go.Figure(data=data, layout=layout)

py.iplot(fig, filename='Diamond')
```

The side of the cube is $ a = 0.3567$ nm. 

1. How is this crystal structure related to the fcc lattice? Compute the basis and one set of primitive lattice vectors.
2. Determine the number of atoms in the primitive unit cell and compute its volume. 

    ??? hint

        Use the primitive lattice vectors you found in (2.1).
        
3. Determine the number of atoms in the conventional unit cell and compute its volume.
4. Using an image of the three-dimensional crystal, determine the number of nearest neighbours for each atom and write down the vectors connecting the nearest neighbours. Which is the nearest neighbour distance?
5. Compute the filling factor. Choose the radius of the atoms to be such that the neighbouring atoms just touch.

### Exercise 3: Directions and Spacings of Miller planes
*(adapted from ex 13.3 of "The Oxford Solid State Basics" by S.Simon)*

1. Explain what is meant by the terms Miller planes and Miller indices.
2. Consider a cubic crystal with one atom in the basis and a set of orthogonal primitive lattice vectors $a\hat{x}$, $a\hat{y}$ and $a\hat{z}$. Show that the direction $[hkl]$ in this crystal is normal to the planes with Miller indices $(hkl)$.
3. Show that this is not true in general. Consider for instance an orthorhombic crystal, for which the primitive lattice vectors are still orthogonal but have different lengths. 
4. Any set of Miller indices corresponds to a family of planes separated by a distance $d$. Show that the spacing $d$ of the $(hkl)$ set of planes in a cubic crystal with lattice parameter $a$ is $d = \frac{a}{\sqrt{h^2 + k^2 + l^2}}$.

    ??? hint

        Recall that a family of lattice planes is an infinite set of equally separated parallel planes which taken all together contain all points of the lattice.

        Try computing the distance between the plane that contains the site $(0,0,0)$ of the conventional unit cell and a plane defined by the $(hkl)$ indices.

